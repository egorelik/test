-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: experimentDB (old)
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Answer`
--

DROP TABLE IF EXISTS `Answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Answer` (
  `ExperimentID` bigint(20) DEFAULT NULL,
  `UserID` bigint(20) DEFAULT NULL,
  `Answer` varchar(50) DEFAULT NULL,
  KEY `FK_Answer_Experiment` (`ExperimentID`),
  CONSTRAINT `FK_Answer_Experiment` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Answer`
--

LOCK TABLES `Answer` WRITE;
/*!40000 ALTER TABLE `Answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `Answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Chat`
--

DROP TABLE IF EXISTS `Chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Chat` (
  `SocialNetworkID` bigint(20) NOT NULL,
  `ChatID` bigint(20) NOT NULL AUTO_INCREMENT,
  `User1ID` bigint(20) NOT NULL,
  `User2ID` bigint(20) NOT NULL,
  `Allowed` int(11) NOT NULL,
  `LogUser1` varchar(50) DEFAULT NULL,
  `LogUser2` varchar(50) DEFAULT NULL,
  `Type` varchar(50) NOT NULL,
  PRIMARY KEY (`ChatID`),
  KEY `FK_Chat_Experiment` (`SocialNetworkID`),
  CONSTRAINT `FK_Chat_Experiment` FOREIGN KEY (`SocialNetworkID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Chat`
--

LOCK TABLES `Chat` WRITE;
/*!40000 ALTER TABLE `Chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `Chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Experiment`
--

DROP TABLE IF EXISTS `Experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Experiment` (
  `ExperimentID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TaskDescription` varchar(50) NOT NULL,
  `CentralQuestion` varchar(50) NOT NULL,
  `CorrectAnswer` varchar(50) NOT NULL,
  `AdditionalInfo` varchar(50) NOT NULL,
  `Owner` bigint(20) NOT NULL,
  `Status` int(11) NOT NULL,
  `Followups` int(11) NOT NULL DEFAULT '0',
  `Predecessor` int(11) DEFAULT NULL,
  PRIMARY KEY (`ExperimentID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Experiment`
--

LOCK TABLES `Experiment` WRITE;
/*!40000 ALTER TABLE `Experiment` DISABLE KEYS */;
/*!40000 ALTER TABLE `Experiment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Languages`
--

DROP TABLE IF EXISTS `Languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Languages` (
  `LanguageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LanguageName` varchar(50) NOT NULL,
  PRIMARY KEY (`LanguageID`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Languages`
--

LOCK TABLES `Languages` WRITE;
/*!40000 ALTER TABLE `Languages` DISABLE KEYS */;
INSERT INTO `Languages` VALUES (1,'English'),(2,'Afar'),(3,'Abkhazian'),(4,'Afrikaans'),(5,'Amharic'),(6,'Arabic'),(7,'Assamese'),(8,'Aymara'),(9,'Azerbaijani'),(10,'Bashkir'),(11,'Belarusian'),(12,'Bulgarian'),(13,'Bihari'),(14,'Bislama'),(15,'Bengali/Bangla'),(16,'Tibetan'),(17,'Breton'),(18,'Catalan'),(19,'Corsican'),(20,'Czech'),(21,'Welsh'),(22,'Danish'),(23,'German'),(24,'Bhutani'),(25,'Greek'),(26,'Esperanto'),(27,'Spanish'),(28,'Estonian'),(29,'Basque'),(30,'Persian'),(31,'Finnish'),(32,'Fiji'),(33,'Faeroese'),(34,'French'),(35,'Frisian'),(36,'Irish'),(37,'Scots/Gaelic'),(38,'Galician'),(39,'Guarani'),(40,'Gujarati'),(41,'Hausa'),(42,'Hindi'),(43,'Croatian'),(44,'Hungarian'),(45,'Armenian'),(46,'Interlingua'),(47,'Interlingue'),(48,'Inupiak'),(49,'Indonesian'),(50,'Icelandic'),(51,'Italian'),(52,'Hebrew'),(53,'Japanese'),(54,'Yiddish'),(55,'Javanese'),(56,'Georgian'),(57,'Kazakh'),(58,'Greenlandic'),(59,'Cambodian'),(60,'Kannada'),(61,'Korean'),(62,'Kashmiri'),(63,'Kurdish'),(64,'Kirghiz'),(65,'Latin'),(66,'Lingala'),(67,'Laothian'),(68,'Lithuanian'),(69,'Latvian/Lettish'),(70,'Malagasy'),(71,'Maori'),(72,'Macedonian'),(73,'Malayalam'),(74,'Mongolian'),(75,'Moldavian'),(76,'Marathi'),(77,'Malay'),(78,'Maltese'),(79,'Burmese'),(80,'Nauru'),(81,'Nepali'),(82,'Dutch'),(83,'Norwegian'),(84,'Occitan'),(85,'(Afan)/Oromoor/Oriya'),(86,'Punjabi'),(87,'Polish'),(88,'Pashto/Pushto'),(89,'Portuguese'),(90,'Quechua'),(91,'Rhaeto-Romance'),(92,'Kirundi'),(93,'Romanian'),(94,'Russian'),(95,'Kinyarwanda'),(96,'Sanskrit'),(97,'Sindhi'),(98,'Sangro'),(99,'Serbo-Croatian'),(100,'Singhalese'),(101,'Slovak'),(102,'Slovenian'),(103,'Samoan'),(104,'Shona'),(105,'Somali'),(106,'Albanian'),(107,'Serbian'),(108,'Siswati'),(109,'Sesotho'),(110,'Sundanese'),(111,'Swedish'),(112,'Swahili'),(113,'Tamil'),(114,'Telugu'),(115,'Tajik'),(116,'Thai'),(117,'Tigrinya'),(118,'Turkmen'),(119,'Tagalog'),(120,'Setswana'),(121,'Tonga'),(122,'Turkish'),(123,'Tsonga'),(124,'Tatar'),(125,'Twi'),(126,'Ukrainian'),(127,'Urdu'),(128,'Uzbek'),(129,'Vietnamese'),(130,'Volapuk'),(131,'Wolof'),(132,'Xhosa'),(133,'Yoruba'),(134,'Chinese'),(135,'Zulu');
/*!40000 ALTER TABLE `Languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Participants`
--

DROP TABLE IF EXISTS `Participants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Participants` (
  `ExperimentID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  KEY `FK_Participants_Experiment` (`ExperimentID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `FK_Participants_Experiment` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Participants`
--

LOCK TABLES `Participants` WRITE;
/*!40000 ALTER TABLE `Participants` DISABLE KEYS */;
/*!40000 ALTER TABLE `Participants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PreliminaryAnswer`
--

DROP TABLE IF EXISTS `PreliminaryAnswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PreliminaryAnswer` (
  `QuestionID` bigint(20) DEFAULT NULL,
  `UserID` bigint(20) DEFAULT NULL,
  `Answer` varchar(50) DEFAULT NULL,
  KEY `FK_PreliminaryAnswer_PreliminiaryQuestions` (`QuestionID`),
  CONSTRAINT `FK_PreliminaryAnswer_PreliminiaryQuestions` FOREIGN KEY (`QuestionID`) REFERENCES `PreliminaryQuestions` (`QuestionID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PreliminaryAnswer`
--

LOCK TABLES `PreliminaryAnswer` WRITE;
/*!40000 ALTER TABLE `PreliminaryAnswer` DISABLE KEYS */;
/*!40000 ALTER TABLE `PreliminaryAnswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PreliminaryQuestions`
--

DROP TABLE IF EXISTS `PreliminaryQuestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PreliminaryQuestions` (
  `QuestionID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ExperimentID` bigint(20) NOT NULL,
  `Question` varchar(50) NOT NULL,
  PRIMARY KEY (`QuestionID`),
  KEY `FK_PreliminiaryQuestions_Experiment` (`ExperimentID`),
  CONSTRAINT `FK_PreliminiaryQuestions_Experiment` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PreliminaryQuestions`
--

LOCK TABLES `PreliminaryQuestions` WRITE;
/*!40000 ALTER TABLE `PreliminaryQuestions` DISABLE KEYS */;
/*!40000 ALTER TABLE `PreliminaryQuestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Schedules`
--

DROP TABLE IF EXISTS `Schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Schedules` (
  `ExperimentID` bigint(20) NOT NULL,
  `BeginExperiment` datetime DEFAULT NULL,
  `BeginDeliberation` datetime DEFAULT NULL,
  `BeginVoting` datetime DEFAULT NULL,
  `EndVoting` datetime DEFAULT NULL,
  KEY `FK_Schedules_Experiment` (`ExperimentID`),
  CONSTRAINT `FK_Schedules_Experiment` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Schedules`
--

LOCK TABLES `Schedules` WRITE;
/*!40000 ALTER TABLE `Schedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `Schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hasLanguages`
--

DROP TABLE IF EXISTS `hasLanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hasLanguages` (
  `ExperimentID` bigint(20) NOT NULL,
  `LanguageID` bigint(20) NOT NULL,
  KEY `FK_hasLanguages_Experiment` (`ExperimentID`),
  KEY `FK_hasLanguages_Languages` (`LanguageID`),
  CONSTRAINT `FK_hasLanguages_Experiment` FOREIGN KEY (`ExperimentID`) REFERENCES `Experiment` (`ExperimentID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hasLanguages_Languages` FOREIGN KEY (`LanguageID`) REFERENCES `Languages` (`LanguageID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hasLanguages`
--

LOCK TABLES `hasLanguages` WRITE;
/*!40000 ALTER TABLE `hasLanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `hasLanguages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-15 10:56:01
